Procurement is responsible for:

* `HWREG_AUTOINSTALL` image. BIOS only
* `PROCUREMENT_AUTOINSTALL` image. UEFI only

These images auto-register the interfaces on LANDB.
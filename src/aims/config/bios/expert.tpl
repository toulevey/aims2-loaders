include @AIMS_CONFIG_PATH@/graphics@MENU_TEXT@.conf

allowoptions 0

menu title Expert Menu for Operating System Installation
F1 @AIMS_CONFIG_PATH/@f1help_default.txt

label advslc
 menu label CERN Centos (CC) / Scientific Linux CERN (SLC) Install
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/advslc@MENU_TEXT@.conf
 text help
  This menu allows manual specification of install time
  parameters. Also installation of test, pre-production
  Scientific Linux CERN (SLC) / CERN CentOS (CC) versions.
 endtext

label advfedora
 menu label Fedora Linux Install Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/advfedora@MENU_TEXT@.conf
  text help
  This menu allows installation of Fedora Linux
  maintained versions http://fedoraproject.org
  Please note that FEDORA linux is NOT SUPPORTED
  at CERN.
 endtext

label advrhel
 menu label Red Hat Enterprise Linux Install Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/advrhel@MENU_TEXT@.conf
 text help
  This menu allows installation of Red Hat Enterprise
  Linux systems (LICENSE REQUIRED).
 endtext

label advbeco
 menu label BE/CO Group Install Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/advbeco@MENU_TEXT@.conf
 text help
  This is installation menu for CERN 
  BO/CO group.
 endtext

label gotodefault
 menu label Return to Main Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/main@MENU_TEXT@.conf
 text help
 Return to the main installation menu.
 endtext

label fromhd
 menu label Boot system from local disk
 menu default
 localboot 0
 text help
  This menu option will boot your system from its local
  disk (or any other local boot device).
 endtext


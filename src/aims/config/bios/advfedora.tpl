include @AIMS_CONFIG_PATH@/graphics@MENU_TEXT@.conf

allowoptions 1

menu title Expert Menu: Fedora Linux installation.
F1 @AIMS_CONFIG_PATH@/f1help_expert.txt

label fedora32_x86_64
 menu label Fedora 32 64-bit system (UNSUPPORTED)
 kernel @AIMS_BOOT_PATH@/FEDORA32_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/FEDORA32_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/fedora/linux/releases/32/Server/x86_64/os/
 text help
  Install Fedora 32 on 64-bit system.
  Please note: Fedora Linux is NOT SUPPORTED AT CERN.
  For information, please see: http://fedoraproject.org/
 endtext

label fedora31_x86_64
 menu label Fedora 31 64-bit system (UNSUPPORTED)
 kernel @AIMS_BOOT_PATH@/FEDORA31_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/FEDORA31_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/fedora/linux/releases/31/Server/x86_64/os/
 text help
  Install Fedora 31 on 64-bit system.
  Please note: Fedora Linux is NOT SUPPORTED AT CERN.
  For information, please see: http://fedoraproject.org/
 endtext

label fedora30_x86_64
 menu label Fedora 30 64-bit system (UNSUPPORTED)
 kernel @AIMS_BOOT_PATH@/FEDORA30_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/FEDORA30_X86_64/initrd ip=dhcp method=http://linuxsoft.cern.ch/fedora/linux/releases/30/Server/x86_64/os/
 text help
  Install Fedora 30 on 64-bit system.
  Please note: Fedora Linux is NOT SUPPORTED AT CERN.
  For information, please see: http://fedoraproject.org/
 endtext

label gotodefault
 menu label Return to Standard Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/main@MENU_TEXT@.conf

label fromhd
 menu label Boot system from local Hard Disk
 menu default
 localboot 0



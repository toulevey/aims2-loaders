include @AIMS_CONFIG_PATH@/graphics@MENU_TEXT@.conf

allowoptions 1

menu title Rescue Boot / Tools
F1 @AIMS_CONFIG_PATH@/f1help_expert.txt

label rescue7
 menu label Rescue Boot the system CC^7 
 kernel @AIMS_BOOT_PATH@/CC7_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/CC7_X86_64/initrd ip=dhcp repo=http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/ rescue
 text help
  Use the rescue boot option in order to recover your damaged system 
  running CentOS CERN 7 (CC7).
 endtext

label rescue62
 menu label Rescue Boot the system SLC^6 64-bit
 kernel @AIMS_BOOT_PATH@/SLC6X_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/SLC6X_X86_64/initrd keymap=us lang=en_US.UTF-8 ip=dhcp method=http://linuxsoft.cern.ch/cern/slc6X/x86_64/ rescue
 text help
  Use the rescue boot option in order to recover your damaged 64-bit system
  running Scientific Linux CERN 6 (SLC6).
 endtext

label rescue61
 menu label Rescue Boot the system SLC6 32-bit
 kernel @AIMS_BOOT_PATH@/SLC6X_I386/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/SLC6X_I386/initrd keymap=us lang=en_US.UTF-8 ip=dhcp noipv6 method=http://linuxsoft.cern.ch/cern/slc6X/i386/ rescue
 text help
  Use the rescue boot option in order to recover your damaged 32-bit system
  running Scientific Linux CERN 6 (SLC6). 
 endtext

label pmagic_x86_64
 menu label Run Parted Magic (2012 02 19) x86_64
 kernel @AIMS_BOOT_PATH@/PARTEDMAGIC_X86_64/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/PARTEDMAGIC_X86_64/initrd iso vmalloc=256M
 text help
  Run Parted Magic OS, suite of disk partitioning programs.
  For more information see: http://www.partedmagic.com/ 
 endtext

label pmagic_i386
 menu label Run Parted Magic (2012 02 19) i386
 kernel @AIMS_BOOT_PATH@/PARTEDMAGIC_I386/vmlinuz
 append initrd=@AIMS_BOOT_PATH@/PARTEDMAGIC_I386/initrd iso vmalloc=256M
 text help
  Run Parted Magic OS, suite of disk partitioning programs.
  For more information see: http://www.partedmagic.com/
 endtext

label gotodefault
 menu label Return to Main Menu
 kernel @AIMS_LOADER_PATH@/@PXE_MENU_BINARY@
 append @AIMS_CONFIG_PATH@/main@MENU_TEXT@.conf
 text help
 Return to the main installation menu.
 endtext

label fromhd
 menu label Boot system from local disk
 menu default
 localboot 0
 text help
  This menu option will boot your system from its local
  disk (or any other local boot device)
 endtext

